use clap::Clap;
use failure::{bail, Error};
use image::{
    imageops::{crop_imm, replace, resize, FilterType},
    GenericImageView, ImageBuffer, Rgb,
};
use rand::seq::SliceRandom;
use std::{collections::HashMap, env::var, path::PathBuf, str::FromStr};
#[macro_use]
extern crate log;

#[derive(Clap, Debug)]
#[clap(name = env!("CARGO_PKG_NAME"), about = "Utility to create grid collages", version = env!("CARGO_PKG_VERSION"))]
struct Args {
    #[clap(
        name = "images",
        about = "The path(s) to the images",
        required = true,
        parse(from_os_str)
    )]
    images: Vec<PathBuf>,
    #[clap(
        short,
        long,
        about = "Enables verbose output, you can also use RUST_LOG env var to set the log level"
    )]
    verbose: bool,
    #[clap(
        short,
        long,
        about = "Path to save the output image to",
        default_value = "out.png",
        parse(from_os_str)
    )]
    output: PathBuf,
    #[clap(
        short,
        long,
        about = "Size of the output image in pixels",
        default_value = "4000:2000"
    )]
    size: Size,
    #[clap(
        short,
        long,
        takes_value = true,
        value_name = "top:left:bottom:right>|<top_and_bottom:right_and_left>|<all",
        about = "Margin around the image in pixels",
        default_value = "0:0:0:0"
    )]
    margin: Margin,
    #[clap(
        short = 'r',
        long,
        takes_value = true,
        about = "Ratio of the images (width/height = ratio) [default: <ratio of the first image>]"
    )]
    images_ratio: Option<f64>,
    #[clap(
        short,
        long,
        takes_value = true,
        value_name = "red:green:blue",
        about = "Color of the background / margin (Range is 0 to 255)",
        default_value = "255:255:255"
    )]
    background_color: BackgroundColor,
    #[clap(
        short = 'c',
        long,
        takes_value = true,
        conflicts_with = "multiplyer",
        required_unless_present = "multiplyer",
        about = "Target number of images"
    )]
    images_count: Option<usize>,
    #[clap(
        short = 'p',
        long,
        takes_value = true,
        conflicts_with = "images-count",
        required_unless_present = "images-count",
        about = "Target multiplyer for the images"
    )]
    multiplyer: Option<usize>,
    #[clap(
        long,
        about = "Disables cropping before scalling, this means that the ratio will get skewed but all the image will be visible"
    )]
    disable_crop: bool,
}

#[derive(Debug)]
struct Size(u32, u32); // width, height

impl FromStr for Size {
    type Err = Error;

    fn from_str(raw: &str) -> Result<Self, Self::Err> {
        let values: Vec<u32> = raw
            .split(':')
            // There is no `try_map`, so this is the best I could come up with to parse and catch the error
            .try_fold(vec![], |mut acc, x| -> Result<Vec<u32>, Error> {
                acc.push(x.parse()?);
                Ok(acc)
            })?;

        Ok(match values.len() {
            2 => Self(values[0], values[1]),
            length => bail!("Size has to have 2 fields, not {}", length),
        })
    }
}

#[derive(Debug)]
struct Margin(u32, u32, u32, u32); // top, left, bottom, right

impl FromStr for Margin {
    type Err = Error;

    fn from_str(raw: &str) -> Result<Self, Self::Err> {
        let values: Vec<u32> = raw
            .split(':')
            // There is no `try_map`, so this is the best I could come up with to parse and catch the error
            .try_fold(vec![], |mut acc, x| -> Result<Vec<u32>, Error> {
                acc.push(x.parse()?);
                Ok(acc)
            })?;

        Ok(match values.len() {
            1 => Self(values[0], values[0], values[0], values[0]),
            2 => Self(values[0], values[1], values[0], values[1]),
            4 => Self(values[0], values[1], values[2], values[3]),
            length => bail!(
                "Margin can eighter have 1, 2 or 4 fields, but not {}",
                length
            ),
        })
    }
}

#[derive(Debug)]
struct BackgroundColor(Rgb<u8>);

impl FromStr for BackgroundColor {
    type Err = Error;

    fn from_str(raw: &str) -> Result<Self, Self::Err> {
        let values: Vec<u8> = raw
            .split(':')
            // There is no `try_map`, so this is the best I could come up with to parse and catch the error
            .try_fold(vec![], |mut acc, x| -> Result<Vec<u8>, Error> {
                acc.push(x.parse()?);
                Ok(acc)
            })?;

        Ok(match values.len() {
            3 => Self(Rgb([values[0], values[1], values[2]])),
            length => bail!("BackgroundColor has to have 3 fields, not {}", length),
        })
    }
}

fn main() -> Result<(), Error> {
    // TODO Implement multiplyer finder (Calculate every multiplyer in a range and display them as a table)
    // TODO Implement dry mode (Generate random solid color images on the fly, waaaay faster)
    // TODO Implement flag to disable the sub image cache (for systems with really low memory)
    // TODO Implement multithreading: This could be done eigher with threadpools or futures.
    //      But this software will peobably not be used much and the fs it will be used on is not fast, so I probably won't be the one adding it.
    // NOTE Performace could probably be improved by using imagemagic(k), but for now having the programm standalone was more important.

    eprint!("  getting ready...\r");

    let args = {
        // Parses the command line arguments into args
        let mut args = Args::parse();

        // Get images_ratio from first image if it isn't already set
        if let None = args.images_ratio {
            args.images_ratio = Some({
                let images_size = image::open(&args.images[0])?.dimensions();

                Ok::<f64, Error>(images_size.0 as f64 / images_size.1 as f64)
            }?);
        }

        // Calculate the image_count when only multiplyer is set...
        if let Some(multiplyer) = args.multiplyer {
            args.images_count = Some(args.images.len() * multiplyer as usize);
        }
        // ...and the other way around
        if let Some(images_count) = args.images_count {
            args.multiplyer =
                Some((images_count as f64 / args.images.len() as f64).ceil() as usize);
        }

        Ok::<Args, Error>(args)
    }?;

    pretty_env_logger::formatted_timed_builder()
        .parse_filters(&match var("RUST_LOG") {
            Ok(val) => val,
            Err(_) => {
                if args.verbose {
                    String::from("debug")
                } else {
                    String::from("info")
                }
            }
        })
        .init();

    debug!("{:#?}", args);

    let image_grid: (usize, usize) = {
        // Same as size, but scaled so that now all images can be treated as quadratic
        let helper_size = (
            args.size.0 as f64 * (1f64 / args.images_ratio.unwrap()),
            args.size.1 as f64,
        );
        let helper_size_ratio = helper_size.0 / helper_size.1;

        // This part is the solution of these equations:
        // x * y == images_count * multiplyer
        // x * helper_size_ratio == y
        (
            (((args.images.len() * args.multiplyer.unwrap()) as f64).sqrt()
                * helper_size_ratio.sqrt())
            .ceil() as usize,
            (((args.images.len() * args.multiplyer.unwrap()) as f64).sqrt()
                / helper_size_ratio.sqrt())
            .ceil() as usize,
        )
    };
    // Size of each image
    // We can't use floats for positions on a bitmap, but we will be using them for compensation
    let images_size: (f64, f64) = (
        (args.size.0 - args.margin.1 - args.margin.3) as f64 / image_grid.0 as f64,
        (args.size.1 - args.margin.0 - args.margin.2) as f64 / image_grid.1 as f64,
    );
    // How many images are needed for the grid (probably more then defined)
    let real_image_count = image_grid.0 * image_grid.1;

    // Warn the user if the grid needs an amount of images that doesn't fit perfectly
    if real_image_count as usize % args.images.len() != 0 {
        let image_descrepancy = (real_image_count as usize
            - args.multiplyer.unwrap() * args.images.len())
            % args.images.len();
        warn!(
            "{} images ({}%) will ocur once more then the over pictures",
            image_descrepancy,
            ((image_descrepancy as f64 / args.images.len() as f64) * 100f64).round()
        );
    }

    // Warn the user if the ratio of the smaller images will be off by more than 0.01
    {
        let final_images_ratio = images_size.0 / images_size.1;
        if (final_images_ratio - args.images_ratio.unwrap()).abs() > 0.01 {
            warn!("Image ratio is differend from final ratio by {} (original: {}, final: {}), the difference will be cut off", (final_images_ratio - args.images_ratio.unwrap()).abs(), args.images_ratio.unwrap(), final_images_ratio);
        }
    }

    // Generate a hashmap of images and positions, the positions are randomly selected
    let image_to_positions: HashMap<&PathBuf, Vec<usize>> = {
        // List of all images (as path) that need to be placed
        let mut images_all: Vec<&PathBuf> = args
            .images
            .iter()
            .cycle()
            .take(real_image_count as usize)
            .collect();
        // Shuffle that list
        images_all.shuffle(&mut rand::thread_rng());
        // Fold the list into a hashmap
        images_all
            .iter()
            .enumerate()
            .fold(HashMap::new(), |mut acc, image| {
                // This removes duplicates and adds them to the list of positions
                if let Some(value) = acc.get_mut(*image.1) {
                    (*value).push(image.0);
                } else {
                    acc.insert(*image.1, vec![image.0]);
                }
                acc
            })
    };

    // Create a new image buffer with the final size and background color
    let mut img = ImageBuffer::from_pixel(args.size.0, args.size.1, args.background_color.0);

    for (i, (path, indexs)) in image_to_positions.iter().enumerate() {
        eprint!(
            "  {:?}/{:?} {:?}/{:?}    \r",
            i + 1,
            image_to_positions.len(),
            0,
            indexs.len()
        );

        debug!("Loading image: {:?}", path);

        // Load and decode the image to be inserted
        let img_sub_full = image::open(path)?.to_rgb8();
        // Create a buffer for saving resized images (only of the current image)
        let mut img_sub_cache: HashMap<(u32, u32), ImageBuffer<Rgb<u8>, Vec<u8>>> = HashMap::new();

        for (j, position_index) in indexs.iter().enumerate() {
            // Calculate offset in pixels and the image size relative to the position
            let (offset, relative_images_size) = {
                let mut relative_images_size = (0u32, 0u32);
                // Used to track where the next image needs to be placed
                let mut offset = (args.margin.1, args.margin.0);
                for i in 0..(*position_index + 1) {
                    // Compensates for not perfect fitting grids (if images_sizes has a fraction part)
                    relative_images_size = (
                        {
                            // If the fraction of the images_size is bigger than the x position in a range from 0 to 1
                            // This makes every image further to the left one pixel bigger so that they better fit into the big picture
                            images_size.0.floor() as u32
                                + if ((i % image_grid.0) as f64 / (image_grid.0 - 1) as f64)
                                    < images_size.0.fract()
                                {
                                    1
                                } else {
                                    0
                                }
                        },
                        {
                            // If the fraction of the images_size is bigger than the y position in a range from 0 to 1
                            // This makes every image further to the left one pixel bigger so that they better fit into the big picture
                            images_size.1.floor() as u32
                                + if ((i / image_grid.0) as f64 / (image_grid.1 - 1) as f64)
                                    < images_size.1.fract()
                                {
                                    1
                                } else {
                                    0
                                }
                        },
                    );

                    // The first image should be at (0, 0) (or rather (margin.0, margin.1)), so don't apply the offset
                    if i != 0 {
                        // If we start a new row
                        if i % image_grid.0 == 0 {
                            // Move one row down
                            offset.1 += relative_images_size.1;
                            // Move to the start of the row
                            offset.0 = args.margin.1;
                        } else {
                            // Move one to the right
                            offset.0 += relative_images_size.0;
                        }
                    }
                }

                (offset, relative_images_size)
            };

            debug!(
                "Placing {:?} on index: {:?}, offset: {:?}, size: {:?}",
                path, position_index, offset, relative_images_size
            );

            eprint!(
                "  {:?}/{:?} {:?}/{:?}    \r",
                i + 1,
                image_to_positions.len(),
                j + 1,
                indexs.len()
            );

            // Replace a section in the final image with the resized image
            replace(
                &mut img,
                {
                    // Check the cache
                    if let None = img_sub_cache.get(&relative_images_size) {
                        // Since the image will probably get a new ratio, it need to be croped before resizing
                        // This takes the shorter side and shortens the other side to fit the new ratio
                        let img_sub_size = img_sub_full.dimensions();
                        let (width, height) =
                            if args.disable_crop || img_sub_size.0 == img_sub_size.1 {
                                (img_sub_size.0 as f64, img_sub_size.1 as f64)
                            } else if img_sub_size.0 > img_sub_size.1 {
                                (
                                    img_sub_size.1 as f64 * relative_images_size.0 as f64
                                        / relative_images_size.1 as f64,
                                    img_sub_size.1 as f64,
                                )
                            } else {
                                (
                                    img_sub_size.0 as f64,
                                    img_sub_size.0 as f64 * relative_images_size.1 as f64
                                        / relative_images_size.0 as f64,
                                )
                            };

                        // Crop and resize the image
                        let img_sub_resized = resize(
                            &crop_imm(
                                &img_sub_full,
                                // Centers the crop
                                ((img_sub_size.0 as f64 - width) / 2f64).floor() as u32,
                                ((img_sub_size.1 as f64 - height) / 2f64).floor() as u32,
                                width.floor() as u32,
                                height.floor() as u32,
                            ),
                            relative_images_size.0,
                            relative_images_size.1,
                            FilterType::Gaussian,
                        );

                        // Put the image into the cache
                        img_sub_cache.insert(relative_images_size, img_sub_resized);
                    }

                    // Get the image reference from the cache
                    img_sub_cache.get(&relative_images_size).unwrap()
                },
                offset.0,
                offset.1,
            );
        }
    }

    debug!("Saving image to: {:?}", args.output);

    eprint!("  saving...    \r");

    // Save image to output path, this will also determine the output format
    img.save(&args.output)?;

    Ok(())
}
